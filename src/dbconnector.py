#Requires pip install mysql-connector-python
import os, time
import mysql.connector
from mysql.connector import Error

dbcredentials = {
    'host'		:	os.environ.get('MYSQL_DATABASE_HOST', 'db'),
	'port'		:	os.environ.get('MYSQL_DATABASE_PORT', 3306),
    'password' 	:	os.environ.get('MYSQL_DATABASE_PASSWORD', 'password'),
    'user'		:	os.environ.get('MYSQL_DATABASE_USER', 'root')
}

#Returns a db connector for the db containing the tasks
def getDBConnection():
    connection = mysql.connector.connect(
        host=dbcredentials['host'],
        port=dbcredentials['port'],
        user=dbcredentials['user'],
        passwd=dbcredentials['password'],
        database="TODOListDB",
        auth_plugin="mysql_native_password"
    )
    return connection
    
        
#Returns a list of all rows retrieved that match the list of tags
#If no tags are given, all tasks are returned
#Does not return tasks that have been closed
def getTasks(tags):
    try:
        mydb = getDBConnection()
    except:
        print("A connection exception occured, DB may not be ready.")
        return None
    cursor = mydb.cursor()
    getStatement = ""
    if tags == None:
        getStatement = ('SELECT Task.id, Task.name, Task.description, Task.date_open ' 
                        'FROM Task '
                        'WHERE date_closed IS NULL')
        cursor.execute(getStatement)
    else:
        tag_strings = ','.join(['%s'] * len(tags))
        getStatement = ('SELECT Task.id, Task.name, Task.description, Task.date_open ' 
                        'FROM Task '
                        'INNER JOIN Task_To_Tag AS Ttg ON Ttg.task_id = Task.id '
                        'INNER JOIN Tag ON Tag.id = Ttg.tag_id '
                        'WHERE date_closed IS NULL AND '
                        'Tag.name IN (%s)'
                        'GROUP BY Task.id '
                        'HAVING COUNT(DISTINCT Tag.name) = %d' % (tag_strings, len(tags)))
        cursor.execute(getStatement, tuple(tags))

    dbReturnData = []
    row = cursor.fetchone()
    while row is not None:
        dbReturnData.append(row)
        row = cursor.fetchone()

    cursor.close()
    mydb.close()
    return dbReturnData


#Removes Task and returns number of rows deleted
#Returns 0 if no rows are deleted or if the todoID is invalid
def removeTask(taskId):
    try:
        mydb = getDBConnection()
    except:
        print("A connection exception occured, DB may not be ready.")
        return None
    try:
        int(taskId)
    except ValueError:
        return 0

    cursor = mydb.cursor()
    delStatement = ('DELETE FROM Task '
                    'WHERE Task.id = %s')
    cursor.execute(delStatement, (taskId,))
    deleteCount = cursor.rowcount

    mydb.commit()
    cursor.close()
    mydb.close()

    return deleteCount


#Adds the given task to the database and adds any tags that are not already present to the Tags table
#Returns the ID of the task that has been added
def addTask(name, description, tags):
    try:
        mydb = getDBConnection()
    except:
        print("A connection exception occured, DB may not be ready.")
        return None
    cursor = mydb.cursor()

    #Insert the Task into the task table
    #mysql connector automatically replaces None type with NULL when inserting into database
    insertStatement = ('INSERT INTO Task (name, description) '
                       'VALUES (%s, %s)')
    cursor.execute(insertStatement, (name, description))
    taskID = cursor.lastrowid

    if(tags is not None):
        for tag in tags:
            #Get tag ID if tag exists in db already
            getTagStatement = ('SELECT Tag.id '
                                'FROM Tag '
                                'WHERE Tag.name = %s')
            cursor.execute(getTagStatement, (tag,))

            tagRow = cursor.fetchone()
            tagId = None
            if tagRow is not None:
                tagId = tagRow[0]
            #Create new tag if tag does not exist in tag table
            else:
                insertTagStatement = ('INSERT INTO Tag (name) '
                                    'VALUES (%s)')
                cursor.execute(insertTagStatement, (tag,))
                tagId = cursor.lastrowid

            #Link Task to Tag in TTG table
            insertTagLinkStatement = ('INSERT INTO Task_To_Tag (task_id, tag_id) '
                                    'VALUES (%s,%s)')
            cursor.execute(insertTagLinkStatement, (taskID, tagId))

    mydb.commit()
    cursor.close()
    mydb.close()
    return taskID

#Update the task given by taskId to the given name, description, and tags
#Existing tags will be overriden with new list of tags, if provided
#if taskCompleted is true, task will be closed
def updateTask(taskId, name, description, tags, taskCompleted):
    try:
        mydb = getDBConnection()
    except:
        print("A connection exception occured, DB may not be ready.")
        return None

    setQueryParams = [] #Stores the string describing the fields to change
    setQueryValues = [] #Stores the values to pass into the params
    if (name is not None):
        setQueryParams.append("name = %s")
        setQueryValues.append(name)
    if (description is not None):
        setQueryParams.append("description = %s")
        setQueryValues.append(description)
    setQueryString = ",".join(setQueryParams)

    cursor = mydb.cursor()

    getUpdatedRowStatement = ('SELECT * FROM Task '
                              'WHERE Task.id = %s')
    cursor.execute(getUpdatedRowStatement, (taskId,))
    if (cursor.fetchone() == None):
        return 0;

    # update name and description if user 
    if (len(setQueryParams) > 0):
        updateStatement = ('UPDATE Task ' 
                           'SET %s '
                           'WHERE Task.id = %%s'% setQueryString)
        cursor.execute(updateStatement, (tuple(setQueryValues) + (taskId,)))

    if (taskCompleted is not None and taskCompleted.lower() == 'true'):
        taskCompleteStatement = ('UPDATE Task '
                                 'SET date_closed = NOW() '
                                 'WHERE Task.id = %s')
        cursor.execute(taskCompleteStatement, (taskId,))
    if (tags is not None):
        # remove current task tags
        getTagStatement = ('DELETE FROM Task_To_Tag '
                           'WHERE Task_To_Tag.task_id = %s')
        cursor.execute(getTagStatement, (taskId,))
        # add new tags
        for tag in tags:
            #Try to get tag ID if tag exists
            getTagStatement = ('SELECT Tag.id '
                                'FROM Tag '
                                'WHERE Tag.name = %s')
            cursor.execute(getTagStatement, (tag,))

            tagRow = cursor.fetchone()
            tagId = None
            if tagRow is not None:
                tagId = tagRow[0]
            else:         #Otherwise create new tag
                insertTagStatement = ('INSERT INTO Tag (name) '
                                    'VALUES (%s)')
                cursor.execute(insertTagStatement, (tag,))
                tagId = cursor.lastrowid

            #Link Task to Tag in TTG table
            insertTagLinkStatement = ('INSERT INTO Task_To_Tag (task_id, tag_id) '
                                    'VALUES (%s,%s)')
            cursor.execute(insertTagLinkStatement, (taskId, tagId))

    getUpdatedRowStatement = ('SELECT * FROM Task '
                              'WHERE Task.id = %s')
    cursor.execute(getUpdatedRowStatement, (taskId,))
    updatedRow = cursor.fetchone()
    mydb.commit()
    cursor.close()
    mydb.close()
    return updatedRow