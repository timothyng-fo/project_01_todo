# Requires pip install flask-restful

from flask import Flask, request, escape
from flask_restful import Resource, Api
import os
try:
    from . import dbconnector as db
except ImportError:
    import dbconnector as db
import json

app = Flask(__name__)
api = Api(app)

apiCredentials = {
    'host'		:	os.environ.get('FLASK_RUN_HOST', '0.0.0.0'),
	'port'		:	os.environ.get('FLASK_RUN_PORT', 5000),
}

key_values = ["taskid", "name", "description", "dateopened", "dateclosed"]


class TodoList(Resource):

    # GET /todo/api/v1/todos
    # GET /todo/api/v1/todos?tags=tag
    # GET /todo/api/v1/todos?tags=tag1,tag2,tag3
    def get(self):
        tags = request.args.get('tags')
        if (tags):
            tags = tags.split(',')

        tasks = db.getTasks(tags)
        if (tasks is None or len(tasks) == 0):
            return 'no tasks found', 200
        
        for i, task in enumerate(tasks):
            returnDict = {}
            for j, field in enumerate(task):
                if (field is not None):
                    returnDict[key_values[j]] = str(field)
            tasks[i] = returnDict
        
        return tasks, 200
        
    # POST /todo/api/v1/todos
    def post(self):
        # required input - task_name
        # optional input - task_description, or tags
        content = request.json
        if (not content or 'task_name' not in content):
            return 'task_name is required', 400
        else:
            tags = content.get('tags')
            if (tags and not isinstance(tags, list)):
                return 'tags must be in json list format', 400

            task_id = db.addTask(content.get('task_name'), content.get('task_description'), tags)
            if (task_id is None):
                print('database not available, try again later')
                return f'database not available, try again later', 204
            return f'task {task_id} added', 201

class Todo(Resource):

    # PUT /todo/api/v1/todos/:todoId
    def put(self, todo_id):
        # at least one parameter is required
        # parameters - task_name, task_description, or tags
        if (not todo_id.isdigit()):
            return f'id {todo_id} is not a number', 400

        content = request.json
        if (not content or ('task_name' not in content
            and 'task_description' not in content
            and 'tags' not in content
            and 'completed' not in content)):
            return 'task_name, task_description, or tags is required', 400
        
        tags = content.get('tags')
        if (tags and not isinstance(tags, list)):
            return 'tags must be in json list format', 400

        updatedTask = db.updateTask(todo_id, content.get('task_name'), content.get('task_description'), tags, content.get('completed'))
        if (updatedTask):
            updatedTask = dict(zip(key_values, map(lambda task_item: str(task_item), updatedTask)))
            return updatedTask, 201
        elif (updatedTask is None):
            print('database not available, try again later')
            return f'database not available, try again later', 204
        else:
            return f'task {todo_id} is invalid', 400
        
    # DELETE /todo/api/v1/todos/:todoId
    def delete(self, todo_id):
        if (not todo_id.isdigit()):
            return f'id {todo_id} is not a number', 400

        deleteCount = db.removeTask(todo_id)
        if (deleteCount):
            return f'deleted task {todo_id}', 201
        elif (deleteCount is None):
            print('database not available, try again later')
            return f'database not available, try again later', 204
        else:
            return f'task {todo_id} is invalid', 400

api.add_resource(TodoList, '/todo/api/v1/todos')
api.add_resource(Todo, '/todo/api/v1/todos/<todo_id>')


if __name__ == '__main__':
    app.run(
        debug=True, 
        host=apiCredentials['host'], 
        port=apiCredentials['port']
    )
