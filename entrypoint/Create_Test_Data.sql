USE TODOListDB;

INSERT INTO Task (name, description) VALUES('Complete Project 1', 'Finish the group project we have been assigned');
SET @task_p1 = LAST_INSERT_ID();
INSERT INTO Task (name, description) VALUES('Finish Apprenticeship Program', 'Complete the entire apprenticeship program and become a fully qualified Software Engineer');
SET @task_apprenticeship = LAST_INSERT_ID();
INSERT INTO Task (name) VALUES('Check-off restaurant bucket list');
SET @task_restaurant = LAST_INSERT_ID();
INSERT INTO Task (name, description) VALUES('Check-off restaurant bucket list', 'Eat ALL the food');
SET @task_restaurantwithdesc = LAST_INSERT_ID();
INSERT INTO Task (name) VALUES('Beat the product analysts at games');
SET @task_games = LAST_INSERT_ID();


INSERT INTO Tag (name) VALUES ('Urgent');
SET @tag_urgent = LAST_INSERT_ID();
INSERT INTO Tag (name) VALUES ('Required');
SET @tag_required = LAST_INSERT_ID();
INSERT INTO Tag (name) VALUES ('Optional');
SET @tag_optional = LAST_INSERT_ID();
INSERT INTO Tag (name) VALUES ('Longterm');
SET @tag_longterm = LAST_INSERT_ID();
INSERT INTO Tag (name) VALUES ('In progress');
SET @tag_inprogress = LAST_INSERT_ID();
INSERT INTO Tag (name) VALUES ('On hold');
SET @tag_onhold = LAST_INSERT_ID();


INSERT INTO Task_To_Tag (task_id, tag_id) VALUES (@task_p1, @tag_urgent);
INSERT INTO Task_To_Tag (task_id, tag_id) VALUES (@task_p1, @tag_required);
INSERT INTO Task_To_Tag (task_id, tag_id) VALUES (@task_p1, @tag_inprogress);

INSERT INTO Task_To_Tag (task_id, tag_id) VALUES (@task_apprenticeship, @tag_required);
INSERT INTO Task_To_Tag (task_id, tag_id) VALUES (@task_apprenticeship, @tag_longterm);
INSERT INTO Task_To_Tag (task_id, tag_id) VALUES (@task_apprenticeship, @tag_inprogress);

INSERT INTO Task_To_Tag (task_id, tag_id) VALUES (@task_restaurant, @tag_optional);
INSERT INTO Task_To_Tag (task_id, tag_id) VALUES (@task_restaurant, @tag_inprogress);

INSERT INTO Task_To_Tag (task_id, tag_id) VALUES (@task_games, @tag_required);
INSERT INTO Task_To_Tag (task_id, tag_id) VALUES (@task_games, @tag_onhold);