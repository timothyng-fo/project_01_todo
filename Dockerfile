FROM python:alpine

ENV FLASK_APP=rest
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_RUN_PORT=5000
ENV FLASK_ENV=development
ENV MYSQL_DATABASE_USER=root
ENV MYSQL_DATABASE_PASSWORD=password
ENV MYSQL_DATABASE_DB=TODOListDB
ENV MYSQL_DATABASE_HOST=db
ENV MYSQL_DATABASE_PORT=3306

RUN apk add build-base 
RUN apk add libffi-dev 
RUN apk add openssl-dev

# Install project dependencies
COPY requirements.txt .
RUN ["pip", "install", "-r", "requirements.txt"]

# Add source code, tests, and test config
COPY ./src .

# Run web app
CMD ["python", "rest.py"]
