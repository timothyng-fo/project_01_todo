import json
import requests

from src.rest import app

initialTasks = [
	"Complete Project 1", 
	"Finish Apprenticeship Program", 
	"Check-off restaurant bucket list", 
	"Check-off restaurant bucket list", 
	"Beat the product analysts at games"
]

TodoListBasePath = 'http://localhost:5000/todo/api/v1/todos'

# GET 		/todo/api/v1/todos
# EXPECTED	200:(initialTasks[0-4])
def test_Get_Incomplete_Tasks():
	with app.test_client() as client:
		response = client.get(
			TodoListBasePath
		)

		apiTasks = list(map(
			lambda task: task['name'],
			json.loads(response.data)
		))

		assert response.status_code == 200
		assert apiTasks == initialTasks

# GET 		/todo/api/v1/todos?tags=In progress
# EXPECTED	200:(initialTasks[0-2])
def test_Get_Tasks_By_One_Tag():
	with app.test_client() as client:
		response = client.get(
			TodoListBasePath, 
			query_string={'tags':'In progress'}
		)

		apiTasks = list(map(
			lambda task: task['name'],
			json.loads(response.data)
		))

		assert response.status_code == 200
		assert apiTasks == initialTasks[:3]

# GET 		/todo/api/v1/todos?tags=Required,In progress
# EXPECTED 	200:(initialTasks[0-1])
def test_Get_Tasks_By_Many_Tags():
	with app.test_client() as client:
		response = client.get(
			TodoListBasePath, 
			query_string={'tags':'Required,In progress'}
		)

		apiTasks = list(map(
			lambda task: task['name'],
			json.loads(response.data)
		))

		assert response.status_code == 200
		assert apiTasks == initialTasks[:2]

# POST 		/todo/api/v1/todos?task_name=Test creation of new to-do item
# EXPECTED 	201:('task 6 added')
def test_Create_New_Task_With_Name():
	with app.test_client() as client:
		response = client.post(
			TodoListBasePath,
			data=json.dumps({
				'task_name':'Test creation of new to-do item'
			}),
            content_type='application/json'
		)

		assert response.status_code == 201
		assert json.loads(response.data) == 'task 6 added'

# POST 		/todo/api/v1/todos
# EXPECTED 	400:('task_name is required')
def test_Create_New_Task_Without_Name():
	with app.test_client() as client:
		response = client.post(
			TodoListBasePath
		)

		assert response.status_code == 400
		assert json.loads(response.data) == 'task_name is required'

# POST 		/todo/api/v1/todos
# EXPECTED 	201:('task 7 added')
def test_Create_New_Task_With_Name_And_Tag():
	with app.test_client() as client:
		response = client.post(
			TodoListBasePath,
			data=json.dumps({
				'task_name':'Test creation of new to-do item with a tag',
				'tags':['For Testing']
			}),
            content_type='application/json'
		)

		assert response.status_code == 201
		assert json.loads(response.data) == 'task 7 added'

# PUT 		/todo/api/v1/todos/4?task_description=Test edit of existing to-do item
# EXPECTED 	201:(initialTasks[3]; updated description)
def test_Edit_Task_Name_By_ID():
	with app.test_client() as client:
		response = client.put(
			TodoListBasePath + '/4',
			data=json.dumps({
				'task_description':'Test edit of existing to-do item',
				'completed':'true'
			}),
			content_type='application/json'
		)

		assert response.status_code == 201
		assert json.loads(response.data)['name'] == initialTasks[3]
		assert json.loads(response.data)['description'] == 'Test edit of existing to-do item'

# PUT 		/todo/api/v1/todos/3?tags=Testing
# EXPECTED 	201:(initialTasks[2])
# GET 		/todo/api/v1/todos?tags=Testing
# EXPECTED 	200:(initialTasks[2])
def test_Edit_Task_Tag_By_ID():
	with app.test_client() as client:
		response = client.put(
			TodoListBasePath + '/3',
			data=json.dumps({
				'tags':['Testing'],
			}),
			content_type='application/json'
		)

		assert response.status_code == 201
		assert json.loads(response.data)['name'] == initialTasks[2]
		
		response = client.get(
			TodoListBasePath,
			query_string={'tags':'Testing'}
		)

		assert response.status_code == 200
		assert json.loads(response.data)[0]['name'] == initialTasks[2]

# PUT 		/todo/api/v1/todos/1
# EXPECTED 	400:('task_name, task_description, or tags is required')
def test_Edit_Task_No_Param_By_ID():
	with app.test_client() as client:
		response = client.put(
			TodoListBasePath + '/1'
		)

		assert response.status_code == 400
		assert json.loads(response.data) == 'task_name, task_description, or tags is required'

# DELETE 	/todo/api/v1/todos/2
# EXPECTED	201:('deleted task 5')
def test_Delete_Task_By_ID():
	with app.test_client() as client:
		response = client.delete(
			TodoListBasePath + '/5'
		)

		assert response.status_code == 201
		assert json.loads(response.data) == 'deleted task 5'

## Exploratory ##

# GET 		/todo/api/v1/todos?tags=test';DROP TABLE `TODOListDB`.`Task`;--
# EXPECTED	200:('no tasks found')
# GET 		/todo/api/v1/todos?tags=Urgent
# EXPECTED	200:(initialTasks[0])
def test_Get_Task_By_One_Tag_SQL_Injection():
	with app.test_client() as client:
		response = client.get(
			TodoListBasePath + "?tags=test';DROP TABLE `TODOListDB`.`Task`;--", 
		)

		assert response.status_code == 200
		assert json.loads(response.data) == 'no tasks found'

		response = client.get(
			TodoListBasePath,
			query_string={'tags':'Urgent'}
		)

		apiTasks = list(map(
			lambda task: task['name'],
			json.loads(response.data)
		))

		assert response.status_code == 200
		assert apiTasks[0] == initialTasks[0]

# GET 		/todo/api/v1/todos?tags=test1,test2';DROP TABLE `TODOListDB`.`Task`;--
# EXPECTED	200:('no tasks found')
# GET 		/todo/api/v1/todos?tags=Required,In progress
# EXPECTED	200:(initialTasks[0-1])
def test_Get_Task_By_Multiple_Tags_SQL_Injection():
	with app.test_client() as client:
		response = client.get(
			TodoListBasePath + "?tags=test1,test2';DROP TABLE `TODOListDB`.`Task`;--", 
		)

		assert response.status_code == 200
		assert json.loads(response.data) == 'no tasks found'

		response = client.get(
			TodoListBasePath,
			query_string={'tags':'Required,In progress'}
		)

		apiTasks = list(map(
			lambda task: task['name'],
			json.loads(response.data)
		))

		assert response.status_code == 200
		assert apiTasks == initialTasks[:2]

# PUT 		/todo/api/v1/todos/-1?task_name=Test edit of negative ID
# EXPECTED	400:('id -1 is not a number')
def test_Edit_Task_Name_By_Negative_ID():
	with app.test_client() as client:
		response = client.put(
			TodoListBasePath + '/-1',
			data=json.dumps({
				'task_name':'Test edit of negative ID'
			}),
			content_type='application/json'
		)

		assert response.status_code == 400
		assert json.loads(response.data) == 'id -1 is not a number'

# PUT 		/todo/api/v1/todos/asdf1?task_name=Test edit of string ID
# EXPECTED	400:('id asdf1 is not a number')
def test_Edit_Task_Name_By_String_ID():
	with app.test_client() as client:
		response = client.put(
			TodoListBasePath + '/asdf1',
			data=json.dumps({
				'task_name':'Test edit of string ID'
			}),
			content_type='application/json'
		)

		assert response.status_code == 400
		assert json.loads(response.data) == 'id asdf1 is not a number'

# PUT 		/todo/api/v1/todos/18446744073709551616?task_name=Test edit of 2^64 ID
# EXPECTED	400:('task 18446744073709551616 is invalid')
def test_Edit_Task_Name_By_Very_Large_ID():
	with app.test_client() as client:
		response = client.put(
			TodoListBasePath + '/18446744073709551616',
			data=json.dumps({
				'task_name':'Test edit of 2^64 ID'
			}),
			content_type='application/json'
		)

		assert response.status_code == 400
		assert json.loads(response.data) == 'task 18446744073709551616 is invalid'

# DELETE 	/todo/api/v1/todos/-1
# EXPECTED	400:('id -1 is not a number')
def test_Delete_Task_By_Negative_ID():
	with app.test_client() as client:
		response = client.delete(
			TodoListBasePath + '/-1'
		)

		assert response.status_code == 400
		assert json.loads(response.data) == 'id -1 is not a number'

# DELETE 	/todo/api/v1/todos/asdf1
# EXPECTED	400:('id asdf1 is not a number')
def test_Delete_Task_By_String_ID():
	with app.test_client() as client:
		response = client.delete(
			TodoListBasePath + '/asdf1'
		)

		assert response.status_code == 400
		assert json.loads(response.data) == 'id asdf1 is not a number'

# DELETE 	/todo/api/v1/todos/18446744073709551616
# EXPECTED	400:('task 18446744073709551616 is invalid')
def test_Delete_Task_By_Very_Large_ID():
	with app.test_client() as client:
		response = client.delete(
			TodoListBasePath + '/18446744073709551616'
		)

		assert response.status_code == 400
		assert json.loads(response.data) == 'task 18446744073709551616 is invalid'

# DELETE 	/todo/api/v1/todos/3
# EXPECTED	201:('deleted task 3')
# DELETE 	/todo/api/v1/todos/3
# EXPECTED	400:('task 3 is invalid')
def test_Delete_Task_Twice_By_ID():
	with app.test_client() as client:
		response = client.delete(
			TodoListBasePath + '/3'
		)

		assert response.status_code == 201
		assert json.loads(response.data) == 'deleted task 3'

		response = client.delete(
			TodoListBasePath + '/3'
		)

		assert response.status_code == 400
		assert json.loads(response.data) == 'task 3 is invalid'

# DELETE 	/todo/api/v1/todos/4
# EXPECTED	201:('deleted task 4')
# PUT 		/todo/api/v1/todos/4?task_name=Buy groceries
# EXPECTED	400:('task 4 is invalid')
def test_Delete_Then_Edit_Task_By_ID():
	with app.test_client() as client:
		response = client.delete(
			TodoListBasePath + '/4'
		)

		assert response.status_code == 201
		assert json.loads(response.data) == 'deleted task 4'

		response = client.put(
			TodoListBasePath + '/4',
			data=json.dumps({
				'task_name':'Buy groceries'
			}),
			content_type='application/json'
		)

		assert response.status_code == 400
		assert json.loads(response.data) == 'task 4 is invalid'

## Exploratory ##